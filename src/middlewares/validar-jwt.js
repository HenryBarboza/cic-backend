const jwt = require('jsonwebtoken');

const validarJWT = (req, res, next) => {

    // Leer el Token
    const token = req.header('x-token');

    console.log(token);

    if ( !token ) {

        console.log('No token');

        return res.status(401).json({
            res: false,
            msg: 'No hay Token en la peticion'
        });
    }

    try {

        const { uid } = jwt.verify( token, process.env.JWT_SECRET);

        // Información que pasó el Meddleware, en este caso el uid añadiendola al req
        req.uid = uid;

        next();

    } catch (error) {
        console.log('Token no válido');
        return res.status(401).json({
            res: false,
            msg: 'Token no válido'
        });
    }

}

module.exports = {
    validarJWT
}
const getMenuFrondtend = ( role = 'EST_ROLE' ) => {
  
  if( role=== 'EST_ROLE' ) {
    const menu = [
      {
        titulo: 'Mis Cursos',
        icono: 'misCursos',
        url: 'estudiante/estCursos',
        submenu: [
          { titulo: 'Inicio', url: '/' },
        ]
      },
      {
        titulo: 'Buscar cursos',
        icono: 'buscarCursos',
        url: 'estudiante/cursosDisp',
        submenu: [
          { titulo: 'Favoritos', url: 'favoritos' },
          { titulo: 'Inscritos', url: 'inscritos' },
        ]
      },
      {
        titulo: 'Perfil',
        icono: 'perfil',
        url: 'estudiante/perfil',
        submenu: [
          { titulo: 'favoritos', url: 'favoritos' },
        ]
      }
    ];
    return menu;
  } else if ( role === 'ADMIN_ROLE' ) {
    const menu = [
      { 
        titulo: 'Dashboard', 
        icono: 'admin',
        url: 'administrador/' 
      }, 
      {
        titulo: 'Cursos', 
        icono: 'buscarCursos',
        url: 'administrador/cursos'
      },
      {
        titulo: 'Usuarios',
        icono: 'users',
        url: 'administrador/usuarios',
      }
    ];
    return menu;

  } else if ( role === 'DOC_ROLE' ) {
    const menu = [
      {
        titulo: 'Mis Cursos',
        icono: 'misCursos',
        url: 'docente',
        submenu: [
          { titulo: 'Inicio', url: '/' },
        ]
      },
      {
        titulo: 'Perfil',
        icono: 'perfil',
        url: 'estudiante/perfil',
        submenu: [
          { titulo: 'favoritos', url: 'favoritos' },
        ]
      }
    ];
    return menu;
  }
}

module.exports = {
  getMenuFrondtend
}
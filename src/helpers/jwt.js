const jwt = require('jsonwebtoken');

// Funcion que genera una promesa y espera el Token

const generarJWT = ( id ) => {

    return new Promise( (resolve, reject ) => {

        // Grabamos en el payload lo que quiera, siempre y cuando no sea info sensible
        const payload = {
            id
        }

        jwt.sign( payload, process.env.JWT_SECRET, {
            expiresIn: '12h'
        }, (err, token ) => {
            
                if ( err ) {
                    console.log(err);
                    reject('No se puedo generar el JWT');
                } else {
                    resolve( token );
                }
            });

        });
    
}

module.exports = {
    generarJWT
}
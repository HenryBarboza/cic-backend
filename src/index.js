require('dotenv').config();
const morgan = require('morgan');
const path = require('path');
const express = require('express');
const cors = require('cors');

const { dbConnection } = require('./database/config');

// Configurando servidor express
const app = express();

// Configurando CORS
app.use( cors() );

// Lectura y parseo del body
app.use( express.json() );
app.use(express.urlencoded({ extended: false }));

// Base de datos
dbConnection();

// settings
app.set('port', process.env.PORT || 3001);


// middlewares
app.use(morgan('dev'));

// routes
app.use('/api/login', require('./routes/auth'));
app.use('/api/usuarioCurso', require('./routes/usuarioCurso'));
app.use('/api/cursos', require('./routes/cursos'));
app.use('/api/usuarios', require('./routes/usuarios'));
app.use('/api/foro', require('./routes/foro'));
app.use('/api/files', require('./routes/files'));
app.use('/api/notificaciones', require('./routes/notificaciones'));
app.use('/api/tema', require('./routes/tema'));
app.use('/api/contenido', require('./routes/contenido'));
app.use('/api/preguntas', require('./routes/preguntas'));
app.use('/api/respuestas', require('./routes/respuestas'));

// Directorio público
app.use(express.static('views'));

// statics files
app.use(express.static(path.join(__dirname, 'public')));


// Lleva directo al Index
app.get('*', (req, resp) => {
    resp.sendFile( path.resolve( __dirname, 'views/index.html') );
});
 
app.listen(app.get('port'), () => {
    console.log('Server on port:', app.get('port'));
});

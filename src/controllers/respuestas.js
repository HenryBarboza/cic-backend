const { response } = require('express');
const admin = require('firebase-admin');

const db = admin.firestore().collection('respuestas');

const getRespuestas = async(req, res) => {
    try {
        const data = await db.get();
        const cont = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            cont
        });
    } catch (error) {
        console.log(error)
    }
}

const getOneRespuesta = async(req, res) => {
    const uid = req.params.id;
    try {
        const Cont = await db.doc(uid).get();

        if(!Cont.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Cont con ese id'
            });
        }

        res.json({
            res: true,
            Cont: Cont.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getFilterRespuestas = async(req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("idPregunta", "==", uid).get();
        const respuestas = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            respuestas
        });

    } catch (error) {
        console.log(error)
    }
}

const crearRespuesta = async(req, res = response) => {

    const respuesta = {
       idPregunta: req.body.idPregunta,
       respuesta: req.body.respuesta,
       estado: req.body.estado
    };

    try {
        const respuestaDB = await db.add(respuesta);

        res.json({
            res: true,
            respuesta: respuestaDB
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

const editarRespuesta = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeRespuesta=  await db.doc(uid).get();

        if(!existeRespuesta.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Cont con ese id'
            });
        }

        const cambiosRespuesta = {
            idPregunta: req.body.idPregunta,
            respuesta: req.body.respuesta,
            estado: req.body.estado
         };

        await db.doc(uid).update(cambiosRespuesta);

        const respuestaEditada =  await db.doc(uid).get();

       res.json({
            res: true,
            repuesta: respuestaEditada.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const editarEstado = async(req, res = response) => {

    const newR = req.params.newResp;
    const prevR = req.params.prevResp;

    try {

        await db.doc(newR).update({estado: true});
        await db.doc(prevR).update({estado: false});

       res.json({
            res: true,
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }

}

const eliminarRespuesta = async(req, res = response) => {
    const uid = req.params.id;

    try {

        const contDB = await db.doc(uid).get();
        
        if(!contDB.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un Cont con ese id'
            })
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'Cont eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getRespuestas,
    getOneRespuesta,
    getFilterRespuestas,
    crearRespuesta,
    editarRespuesta,
    editarEstado,
    eliminarRespuesta
}
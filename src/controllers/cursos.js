const { response } = require('express');
const admin = require('firebase-admin');
const { get } = require('../routes/cursos');

const db = admin.firestore().collection('cursos');

const getCursos = async(req, res) => {
    try {
        const data = await db.get();
        const cursos = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            cantidad: cursos.length,
            res: true,
            cursos
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}
const countCursos = async(req, res) => {
    try {
        const data = await db.get();
        const cursos = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            cantidad: cursos.length,
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getOneCurso = async(req, res) => {

    const uid = req.params.id;

    try {
        const Curso =  await db.doc(uid).get();

        if(!Curso.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe curso con ese id'
            });
        }

        res.json({
            res: true,
            curso: Curso.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getCursosDocente = async(req, res = response) => {

    const uid = req.params.id;
    
    try {
        const getCursos = await db.where('idDocente', "==", uid).get();
        const cursos = getCursos.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            cursos 
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const crearCurso = async(req, res = response) => {

    const curso = {
        titulo: req.body.titulo,
        descripcion: req.body.descripcion,
        aprender: req.body.aprender,
        requerimientos: req.body.requerimientos,
        image: req.body.image,
        fechaInicio: req.body.fechaInicio,
        precio: req.body.precio,
        cantEstudiantes: 0,
        likes: 0,
        idDocente: req.body.idDocente
    };

    try {
        const cursoBD = await db.add(curso);

        res.json({
            res: true,
            curso: cursoBD
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }

}

const editarCurso = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeCurso =  await db.doc(uid).get();

        if(!existeCurso.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe curso con ese id'
            });
        }

        const cambiosCurso = {
            titulo: req.body.titulo,
            descripcion: req.body.descripcion,
            aprender: req.body.aprender,
            requerimientos: req.body.requerimientos,
            image: req.body.image,
            fechaInicio: req.body.fechaInicio,
            precio: req.body.precio,
            cantEstudiantes: 0,
            likes: 0,
            idDocente: req.body.idDocente
        }

        await db.doc(uid).update(cambiosCurso);

        const cursoEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            curso: cursoEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }

}

const eliminarCurso = async(req, res = response) => {

    const uid = req.params.id;

    try {

        const cursoBD = await db.doc(uid).get();
        
        if(!cursoBD.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un curso con ese id'
            });
        }

        const getTemas = await admin.firestore().collection('temas').where("idCurso", "==", uid).get();
        const temas = getTemas.docs.map(doc => ({id: doc.id, ...doc.data()}));
        
        for ( i = 0; i < temas.length; i++ ) {
 
            const getContenido = await admin.firestore().collection('contenido').where("idTema", "==", temas[i].id).get();
            const contenido = getContenido.docs.map(doc => ({id: doc.id, ...doc.data()}));
            
            for ( j = 0; j < contenido.length; j++ ) {

                await admin.firestore().collection('contenido').doc(contenido[j].id).delete();

            }

            await admin.firestore().collection('temas').doc(temas[i].id).delete();

        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'Curso eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }

}


module.exports = {
    getCursos,
    countCursos,
    getOneCurso,
    getCursosDocente,
    crearCurso,
    editarCurso,
    eliminarCurso
}
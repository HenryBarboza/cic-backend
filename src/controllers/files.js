const { response } = require('express');
const admin = require('firebase-admin');

const db = admin.firestore().collection('files');

const getFile = async(req, res) => {
    try {
        const data = await db.get();
        const files = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            files
        });
    } catch (error) {
        console.log(error)
    }
}

const getFileOne = async(req, res) => {
    const uid = req.params.id;
    try {
        const File = await db.doc(uid).get();

        if(!File.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe File con ese id'
            });
        }

        res.json({
            res: true,
            File: File.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getFilterFile = async(req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("idForo", "==", uid).get();
        const file = data.docs.map(doc => ({id: doc.id, ...doc.data()}));
        /* userId = foro.map((c) => {
            const user = bd.where();
        })     */    

        res.json({
            res: true,
            file: file
        });

    } catch (error) {
        console.log(error)
    }
}

const crearFile = async(req, res = response) => {
    const file = {
        idForo: req.body.idForo,
        enlace: req.body.enlace,
        nombre: req.body.nombre,
        tipo: req.body.tipo
    };

    try {
        const fileDB = await db.add(file);

        res.json({
            res: true,
            file: fileDB
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

const editarFile = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeFile =  await db.doc(uid).get();

        if(!existeFile.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe file con ese id'
            });
        }

        const cambiosFile = {
            idForo: req.body.idForo,
            enlace: req.body.enlace,
            nombre: req.body.nombre,
            tipo: req.body.tipo
        }

        await db.doc(uid).update(cambiosFile);

        const fileEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            file: fileEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const eliminarFile = async(req, res = response) => {
    const uid = req.params.id;

    try {

        const fileDB = await db.doc(uid).get();
        
        if(!fileDB.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un file con ese id'
            })
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'File eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getFile,
    getFileOne,
    getFilterFile,
    crearFile,
    editarFile,
    eliminarFile
}
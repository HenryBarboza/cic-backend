const { response } = require('express');
const admin = require('firebase-admin');

const db = admin.firestore().collection('preguntas');

const getPreguntas = async (req, res) => {
    try {
        const data = await db.get();
        const preguntas = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            preguntas
        });
    } catch (error) {
        console.log(error)
    }
}

const getOnePregunta = async (req, res) => {
    const uid = req.params.id;
    try {
        const Cont = await db.doc(uid).get();

        if(!Cont.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Cont con ese id'
            });
        }

        res.json({
            res: true,
            Preguntas: Cont.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getFilterPreguntas = async (req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("idTema", "==", uid).get();
        const preguntas = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            preguntas
        });

    } catch (error) {
        console.log(error)
    }
}

const crearPregunta = async (req, res = response) => {
    const cont = {
       idTema: req.body.idTema,
       pregunta: req.body.pregunta
    };

    try {
        const contDB = await db.add(cont).then((docRef) => { return docRef.id})


        res.json({
            res: true,
            idPreguntaCreada: contDB
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

const editarPregunta = async (req, res = response) => {

    const uid = req.params.id;

    try {
        const existePregunta =  await db.doc(uid).get();

        if(!existePregunta.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Pregunta con ese id'
            });
        }

        const cambiosCont = {
            idTema: req.body.idTema,
            pregunta: req.body.pregunta
        }

        await db.doc(uid).update(cambiosCont);

        const contEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            pregunta: contEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const eliminarPregunta = async (req, res = response) => {
    const uid = req.params.id;

    try {

        const contDB = await db.doc(uid).get();
        
        if(!contDB.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un Cont con ese id'
            })
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'Cont eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getPreguntas,
    getOnePregunta,
    getFilterPreguntas,
    crearPregunta,
    editarPregunta,
    eliminarPregunta
}
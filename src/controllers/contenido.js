const { response } = require('express');
const admin = require('firebase-admin');

const db = admin.firestore().collection('contenido');

const getCont = async(req, res) => {
    try {
        const data = await db.get();
        const cont = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            cont
        });
    } catch (error) {
        console.log(error)
    }
}

const getContOne = async(req, res) => {
    const uid = req.params.id;
    try {
        const Cont = await db.doc(uid).get();

        if(!Cont.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Cont con ese id'
            });
        }

        res.json({
            res: true,
            Cont: Cont.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getFilterCont = async(req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("idTema", "==", uid).orderBy("indice", "asc").get();
        const cont = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            cantidad: cont.length,
            res: true,
            cont
        });

    } catch (error) {
        console.log(error)
    }
}

const crearCont = async(req, res = response) => {
    const cont = {
       idTema: req.body.idTema,
       indice: Number(req.body.indice),
       titulo: req.body.titulo,
       descripcion: req.body.descripcion,
       file: req.body.file
    };

    try {
        const contDB = await db.add(cont);

        res.json({
            res: true,
            cont: contDB
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

const editarCont = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeCont =  await db.doc(uid).get();

        if(!existeCont.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Cont con ese id'
            });
        }

        const cambiosCont = {
            idTema: req.body.idTema,
            indice: Number(req.body.indice),
            titulo: req.body.titulo,
            descripcion: req.body.descripcion,
            file: req.body.file
        }

        await db.doc(uid).update(cambiosCont);

        const contEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            contenido: contEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const eliminarCont = async(req, res = response) => {
    const uid = req.params.id;

    try {

        const contDB = await db.doc(uid).get();
        
        if(!contDB.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un Cont con ese id'
            })
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'Cont eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getCont,
    getContOne,
    getFilterCont,
    crearCont,
    editarCont,
    eliminarCont
}
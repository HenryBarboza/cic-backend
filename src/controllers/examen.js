const { response } = require('express');
const admin = require('firebase-admin');

const db = admin.firestore().collection('examen');

const getExamen = async (req, res) => {
    try {
        const data = await db.get();
        const preguntas = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            preguntas
        });
    } catch (error) {
        console.log(error)
    }
}

const getOneExamen = async (req, res) => {
    const uid = req.params.id;
    try {
        const Cont = await db.doc(uid).get();

        if(!Cont.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Cont con ese id'
            });
        }

        res.json({
            res: true,
            Preguntas: Cont.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getFilterExamen = async (req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("idTema", "==", uid).get();
        const preguntas = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            preguntas
        });

    } catch (error) {
        console.log(error)
    }
}

const crearExamen = async (req, res = response) => {

    const examen = {
       idTema: req.body.idTema,
       descripcion: req.body.descripcion
    };

    try {
        const examenBD = await db.add(examen).then((docRef) => { return docRef.id});

        res.json({
            res: true,
            examen: examenBD
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

const editarExamen = async (req, res = response) => {

    const uid = req.params.id;

    try {
        const existeExamen =  await db.doc(uid).get();

        if(!existeExamen.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Examen con ese id'
            });
        }

        const cambiosExamen = {
        idTema: req.body.idTema,
        descripcion: req.body.descripcion
        };

        await db.doc(uid).update(cambiosExamen);

        const examenEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            examen: examenEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const eliminarExamen = async (req, res = response) => {

    const uid = req.params.id;

    try {

        const examenDB = await db.doc(uid).get();
        
        if(!examenDB.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un Examen con ese id'
            })
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'Examen eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getExamen,
    getOneExamen,
    getFilterExamen,
    crearExamen,
    editarExamen,
    eliminarExamen
}
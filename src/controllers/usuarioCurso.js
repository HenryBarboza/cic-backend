const { response } = require('express');
const admin = require('firebase-admin');

const db = admin.firestore().collection('usuarioCurso');

const getUsuarioCurso = async(req, res) => {

    try {
        const data = await db.get();
        const usuarioCurso = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            usuarioCurso,
        });

    } catch (error) {
        console.log(error)
    }
}
const filterUsuarios = async(req, res ) => {

    const cursoId= req.params.id;

try {
    const data = await db.where("cursoId", "==", cursoId).where("estado", "==", 1).get();
    const usuarioCurso = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

    res.json({
        cantidad: usuarioCurso.length,
        res: true,
        msg: 'el usuario ya existe',
        estado: usuarioCurso[0].estado,
        colect: usuarioCurso
    });

} catch (error) {
    console.log(error);
}
}

const getUsuarioCursoOne = async(req, res) => {
    const uid = req.params.id;
    try {
        const usuarioCurso = await db.doc(uid).get();

        if(!usuarioCurso.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe usuarioCurso con ese id'
            });
        }

        res.json({
            res: true,
            usuarioCurso: usuarioCurso.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getFilterUsuarioCurso = async(req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("userId", "==", uid).where("estado", "==", 1).get();
        const usuarioCurso = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            usuarioCurso: usuarioCurso,
        });

    } catch (error) {
        console.log(error)
    }
}

const getFilterUsuarioCursoIds = async(req, res = response ) => {

        const userId= req.body.userId;
        const cursoId= req.body.cursoId;

    try {
        const data = await db.where("userId", "==", userId).where("cursoId", "==", cursoId).get();
        const usuarioCurso = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            msg: 'el usuario ya existe',
            estado: usuarioCurso[0].estado,
            colect: usuarioCurso
        });

    } catch (error) {
        console.log(error);
    }
}


const crearUsuarioCurso = async(req, res = response) => {
    const usuarioCurso = {
        userId: req.body.userId,
        cursoId: req.body.cursoId,
        estado: req.body.estado,
    };

    try {

        const usuarioCursoDB = await db.add(usuarioCurso);

        res.json({
            res: true,
            usuarioCurso: usuarioCursoDB
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

const editarUsuarioCurso = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeUsuarioCurso =  await db.doc(uid).get();

        if(!existeUsuarioCurso.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe usuarioCurso con ese id'
            });
        }

        const cambiosUsuarioCurso = {
            userId: req.body.userId,
            cursoId: req.body.cursoId,
        }

        await db.doc(uid).update(cambiosUsuarioCurso);

        const usuarioCursoEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            curso: usuarioCursoEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const eliminarUsuarioCurso = async(req, res = response) => {
    const uid = req.params.id;

    try {

        const usuarioCursoDB = await db.doc(uid).get();
        
        if(!usuarioCursoDB.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un usuarioCurso con ese id'
            })
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'usuarioCurso eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getUsuarioCurso,
    getUsuarioCursoOne,
    filterUsuarios,
    getFilterUsuarioCurso,
    getFilterUsuarioCursoIds,
    crearUsuarioCurso,
    editarUsuarioCurso,
    eliminarUsuarioCurso
}
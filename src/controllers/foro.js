const { response } = require('express');
const admin = require('firebase-admin');

const db = admin.firestore().collection('foro');

const getForo = async(req, res) => {
    try {
        const data = await db.get();
        const foro = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            foro
        });
    } catch (error) {
        console.log(error)
    }
}

const getForoOne = async(req, res) => {
    const uid = req.params.id;
    try {
        const Foro = await db.doc(uid).get();

        if(!Foro.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Foro con ese id'
            });
        }

        res.json({
            res: true,
            Foro: Foro.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getFilterForo = async(req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("idPadre", "==", uid).orderBy("fecha", "desc").get();
        const foro = data.docs.map(doc => ({id: doc.id, ...doc.data()}));
        /*userId = foro.map((c) => {
            const user = bd.where();
        })*/
        res.json({
            res: true,
            foro
        });

    } catch (error) {
        console.log(error);
    }
}

const crearForo = async(req, res = response) => {
    const foro = {
        idUser: req.body.idUser,
        fecha: req.body.fecha,
        texto: req.body.texto,
        idPadre: req.body.idPadre,
    };

    try {
        const foroDB = await db.add(foro);

        res.json({
            res: true,
            foro: foroDB.id
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

const editarForo = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeForo =  await db.doc(uid).get();

        if(!existeForo.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe foro con ese id'
            });
        }

        const cambiosForo = {
            idUser: req.body.idUser,
            fecha: req.body.fecha,
            texto: req.body.texto,
            idPadre: req.body.idPadre,
            files: req.body.files
        }

        await db.doc(uid).update(cambiosForo);

        const foroEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            curso: foroEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const eliminarForo = async(req, res = response) => {
    const uid = req.params.id;

    try {

        const foroDB = await db.doc(uid).get();
        
        if(!foroDB.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un foro con ese id'
            })
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'Foro eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getForo,
    getForoOne,
    getFilterForo,
    crearForo,
    editarForo,
    eliminarForo
}
const { response } = require('express');
const admin = require('firebase-admin');

const db = admin.firestore().collection('temas');

const getTema = async(req, res) => {
    try {
        const data = await db.get();
        const tema = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            cantidad: tema.length,
            res: true,
            tema
        });
    } catch (error) {
        console.log(error)
    }
}

const getTemaOne = async(req, res) => {
    const uid = req.params.id;
    try {
        const tema = await db.doc(uid).get();

        if(!tema.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe tema con ese id'
            });
        }

        res.json({
            res: true,
            tema: tema.data()
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getFilterTema = async(req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("idCurso", "==", uid).orderBy("indice", "asc").get();
        const tema = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            res: true,
            tema
        });

    } catch (error) {
        console.log(error)
    }
}

const crearTema = async(req, res = response) => {
    const tema = {
        idCurso: req.body.idCurso,
        indice: Number(req.body.indice),
        titulo: req.body.titulo,
        contenido: req.body.contenido
    };

    try {
        const temaDB = await db.add(tema);

        res.json({
            res: true,
            tema: temaDB
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

const editarTema = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeTema =  await db.doc(uid).get();

        if(!existeTema.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Tema con ese id'
            });
        }

        const cambiosTema = {
            idCurso: req.body.idCurso,
            indice: Number(req.body.indice),
            titulo: req.body.titulo,
            contenido: req.body.contenido
        }

        await db.doc(uid).update(cambiosTema);

        const temaEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            curso: temaEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const eliminarTema = async(req, res = response) => {
    const uid = req.params.id;

    try {

        const temaDB = await db.doc(uid).get();
        
        if(!temaDB.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un tema con ese id'
            })
        }

        const data = await admin.firestore().collection('contenido').where("idTema", "==", uid).get();
        const contenido = data.docs.map(doc => ({id: doc.id, ...doc.data()}));
        
        for(i = 0; i < contenido.length; i++) {
            await admin.firestore().collection('contenido').doc(contenido[i].id).delete();
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'tema eliminado existosamente',
        });


    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getTema,
    getTemaOne,
    getFilterTema,
    crearTema,
    editarTema,
    eliminarTema
}
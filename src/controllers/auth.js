const { response } = require('express');
const admin = require('firebase-admin');
const bcrypt  = require('bcryptjs');

const { generarJWT } = require('../helpers/jwt');
const { getMenuFrondtend } = require('../helpers/menu-frondtend');

const db = admin.firestore().collection('usuarios');

const login = async( req, res = response ) => {
    
    const { email, pwd} = req.body;

    try {

        // Verificar si existe el email
        const existeEmail =  await db.where('email', '==', email).get();
        const usuario = existeEmail.docs.map(doc => ({id: doc.id, ...doc.data()}));

        if(usuario.length === 0) {
            return res.json({
                res: false,
                msg: 'Email no encontrado'
            });
        }

        // Verifcar contraseña
        const validPassword = bcrypt.compareSync( pwd, usuario[0].password );

        // Verificar constraseña
        if(!validPassword) {
            return res.status(400).json({
                res: false,
                msg: 'Contraseña no válida'
            });
        }

        const { password, ...user } = usuario[0];

        // Generar token
        const token = await generarJWT( usuario[0].id );


        res.status(200).json({
            res:true,
            token,
            user,
            menu: getMenuFrondtend( usuario[0].rol )

        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error, hable con el administrador'
        });
    }
} 

module.exports = {
    login
}
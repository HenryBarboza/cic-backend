const { response } = require('express');
const admin = require('firebase-admin');
const bcrypt  = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt');
const { getMenuFrondtend } = require('../helpers/menu-frondtend');

const db = admin.firestore().collection('usuarios');

const getUsuarios = async(req, res) => {

    try {
        const data = await db.get();
        const usuarios = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            cantidad: usuarios.length,
            res: true,
            usuarios
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getRole = async(req, res) => {

    const role = req.params.role;
    try {
        const data = await db.where("rol", "==", role).get();
        const usuarios = data.docs.map(doc => ({id: doc.id, ...doc.data()}));

        res.json({
            cantidad: usuarios.length,
            res: true,
            usuarios
        });

    } catch (error) {

        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const getOneUsuario = async(req, res) => {
    const uid = req.params.id;
    try {
        const Usuario = await db.doc(uid).get();
        
        if(!Usuario.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe Usuario con ese id'
            });
        }
        
        res.json({
            res: true,
            usuario: Usuario.data()
        });
        
    } catch (error) {
        
        console.log(error);
        return res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
    }
}

const crearUsuario = async(req, res = response) => {

    const usuario = {
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password,
        rol: 'EST_ROLE'
    };

    try {

        // Verificar email duplicado
        const existeEmail =  await db.where('email', '==', usuario.email).get();
        const arrayData = existeEmail.docs.map(doc => ({id: doc.id, ...doc.data()}));

        if(arrayData.length > 0) {
            return res.json({
                res: false,
                msg: 'El email ya está registrado'
            })
        }

        // Encriptar constraseña
        //Hash de una sola via
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync( usuario.password, salt );


        // Guardar usuario
        await db.add(usuario);

        // Recuperar usuario para añadir token
        const getUser =  await db.where('email', '==', usuario.email).get();
        const user = getUser.docs.map(doc => ({id: doc.id, ...doc.data()}));

        // Generar token
        const token = await generarJWT( user[0].id );

        res.status(200).json({
            res: true,
            token,
            user: user[0],
            menu: getMenuFrondtend( user[0].rol )
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            res: false,
            msg: 'Error inesperado... revisar logs'
        });
    }

}

const editarUsuario = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeUser =  await db.doc(uid).get();

        if(!existeUser.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe usuario con ese id'
            });
        }

        const cambiosUser = {
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            contacto: req.body.contacto
        }

        await db.doc(uid).update(cambiosUser);

        const userEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            curso: userEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const editarRol = async(req, res = response) => {

    const uid = req.params.id;

    try {
        const existeUser =  await db.doc(uid).get();

        if(!existeUser.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe usuario con ese id'
            });
        }

        const cambiosUser = {
            rol: req.body.rol,
        }

        await db.doc(uid).update(cambiosUser);

        const userEditado =  await db.doc(uid).get();

       res.json({
            res: true,
            curso: userEditado.data()
        });

    } catch (error) {

        res.status(500).json({
            res: false,
            msg: 'Hable con el administrador'
        });
        
    }
}

const getFilterUser = async(req, res) => {

    const uid = req.params.id;

    try {
        const data = await db.where("id", "==", uid).get();
        const user = data.docs.map(doc => ({id: doc.id, ...doc.data()}));
        /* userId = tema.map((c) => {
            const user = bd.where();
        })     */    

        res.json({
            cantidad: user.length,
            res: true,
            user
        });

    } catch (error) {
        console.log(error)
    }
}

const eliminarUsuario = async(req, res = response) => {

    const uid = req.params.id;

    try {

        const userBD = await db.doc(uid).get();
        
        if(!userBD.data()) {
            return res.status(404).json({
                res: false,
                msg: 'No existe un usuario con ese id'
            })
        }

        await db.doc(uid).delete();

        res.json({
            res: true,
            msg: 'Usuario eliminado existosamente'
        });

    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }

}

module.exports = {
    getUsuarios,
    getRole,
    getOneUsuario,
    crearUsuario,
    editarUsuario,
    editarRol,
    getFilterUser,
    eliminarUsuario
}
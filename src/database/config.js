const admin = require('firebase-admin');

const dbConnection = async() => {
    try {
        const serviceAccount = require('../../cic-pasantia-firebase-adminsdk-pr7vk-9838119bea.json');

        await admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: 'https://cic-pasantia-default-rtdb.firebaseio.com'
        });
        
        console.log('DB Online');

    } catch (error) {
        console.log(error);
        throw new Error('Error a la hora de iniciar la BD ver logs')
    }
}

module.exports = {
    dbConnection
}

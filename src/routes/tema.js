const { Router } = require('express');
const { getTema, crearTema, editarTema, eliminarTema, getTemaOne, getFilterTema } = require('../controllers/tema');
const { validarJWT } = require('../middlewares/validar-jwt');


const router = Router();


router.get('/', getTema);

router.get('/:id', getTemaOne);

router.get('/filter/:id', getFilterTema);

router.post('/', validarJWT, crearTema);

router.put('/edit/:id', validarJWT, editarTema);

router.delete('/delete/:id', eliminarTema);

module.exports = router;
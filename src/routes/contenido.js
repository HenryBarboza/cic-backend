const { Router } = require('express');
const { getCont, crearCont, editarCont, eliminarCont, getContOne, getFilterCont } = require('../controllers/contenido');
const { validarJWT } = require('../middlewares/validar-jwt');


const router = Router();


router.get('/', getCont);

router.get('/:id', getContOne);

router.get('/filter/:id', getFilterCont);

router.post('/', validarJWT, crearCont);

router.put('/edit/:id', validarJWT, editarCont);

router.delete('/delete/:id', eliminarCont);

module.exports = router;
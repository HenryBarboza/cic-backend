const { Router } = require('express');
const { getExamen, getOneExamen, getFilterExamen, crearExamen, editarExamen, eliminarExamen } = require('../controllers/examen');

const router = Router();


router.get('/', getExamen);

router.get('/:id', getOneExamen);

router.get('/filter/:id', getFilterExamen);

router.post('/', crearExamen);

router.put('/edit/:id', editarExamen);

router.delete('/delete/:id', eliminarExamen);

module.exports = router;
const { Router } = require('express');
const { getFile, crearFile, editarFile, eliminarFile, getFileOne, getFilterFile } = require('../controllers/files');


const router = Router();


router.get('/', getFile);

router.get('/:id', getFileOne);

router.get('/filter/:id', getFilterFile);

router.post('/', crearFile);

router.put('/edit/:id', editarFile);

router.delete('/delete/:id', eliminarFile);

module.exports = router;
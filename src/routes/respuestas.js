const { Router } = require('express');
const { eliminarRespuesta, editarRespuesta, editarEstado, crearRespuesta, getFilterRespuestas, getOneRespuesta, getRespuestas } = require('../controllers/respuestas');

const router = Router();


router.get('/', getRespuestas);

router.get('/:id', getOneRespuesta);

router.get('/filter/:id', getFilterRespuestas);

router.post('/', crearRespuesta);

router.put('/edit/:id', editarRespuesta);

router.put('/editEstado/:newResp/:prevResp', editarEstado);

router.delete('/delete/:id', eliminarRespuesta);

module.exports = router;
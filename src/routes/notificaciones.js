const { Router } = require('express');
const { getNotif, crearNotif, editarNotif, eliminarNotif, getNotifOne, getFilterNotif } = require('../controllers/notificaciones');


const router = Router();


router.get('/', getNotif);

router.get('/:id', getNotifOne);

router.get('/filter/:id', getFilterNotif);

router.post('/', crearNotif);

router.put('/edit/:id', editarNotif);

router.delete('/delete/:id', eliminarNotif);

module.exports = router;
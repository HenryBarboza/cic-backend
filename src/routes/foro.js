const { Router } = require('express');
const { getForo, crearForo, editarForo, eliminarForo, getForoOne, getFilterForo } = require('../controllers/foro');


const router = Router();


router.get('/', getForo);

router.get('/:id', getForoOne);

router.get('/filter/:id', getFilterForo);

router.post('/', crearForo);

router.put('/edit/:id', editarForo);

router.delete('/delete/:id', eliminarForo);

module.exports = router;
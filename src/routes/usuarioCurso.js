const { Router } = require('express');
const { 
        getUsuarioCurso,
        getUsuarioCursoOne,
        getFilterUsuarioCurso,
        getFilterUsuarioCursoIds,
        filterUsuarios,
        crearUsuarioCurso,
        editarUsuarioCurso,
        eliminarUsuarioCurso
        } = require('../controllers/usuarioCurso');

const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();


router.get('/', getUsuarioCurso);

router.get('/:id', getUsuarioCursoOne);

router.get('/filter/:id', getFilterUsuarioCurso);

router.get('/filterUsers/:id', filterUsuarios);

router.post('/filterIds/', getFilterUsuarioCursoIds);

router.post('/', crearUsuarioCurso);

router.put('/edit/:id', editarUsuarioCurso);

router.delete('/delete/:id', eliminarUsuarioCurso);

module.exports = router;
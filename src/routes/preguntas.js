const { Router } = require('express');
const { getPreguntas, getOnePregunta, getFilterPreguntas, editarPregunta, crearPregunta, eliminarPregunta } = require('../controllers/preguntas');
const { validarJWT } = require('../middlewares/validar-jwt');


const router = Router();


router.get('/', getPreguntas);

router.get('/:id', getOnePregunta);

router.get('/filter/:id', getFilterPreguntas);

router.post('/', crearPregunta);

router.put('/edit/:id', editarPregunta);

router.delete('/delete/:id', eliminarPregunta);

module.exports = router;
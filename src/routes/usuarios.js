const { Router } = require('express');

const { 
        crearUsuario,
        getUsuarios,
        getRole,
        getOneUsuario,
        editarUsuario,
        editarRol,
        getFilterUser,
        eliminarUsuario
    } = require('../controllers/usuarios');

const router = Router();

router.get('/', getUsuarios);

//router.get('/', validarJWT, getUsuarios);

router.get('/rol/:role', getRole);

router.get('/:id', getOneUsuario);

router.post('/', crearUsuario);

router.put('/edit/:id', editarUsuario);

router.put('/editRol/:id', editarRol);

router.put('/filter/:id', getFilterUser);

router.delete('/', eliminarUsuario);

module.exports = router;
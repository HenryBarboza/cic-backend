const { Router } = require('express');
const { getCursos, countCursos, crearCurso, editarCurso, eliminarCurso, getOneCurso, getCursosDocente } = require('../controllers/cursos');
const { validarJWT } = require('../middlewares/validar-jwt');


const router = Router();


router.get('/', getCursos);

router.get('/contar/', countCursos);

router.get('/:id', getOneCurso);

router.get('/docente/:id', getCursosDocente);

router.post('/', validarJWT, crearCurso);

router.put('/:id',validarJWT, editarCurso);

router.delete('/:id', eliminarCurso);

module.exports = router;